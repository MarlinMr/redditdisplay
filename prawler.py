import serial
import praw
import sys
baudRate=9600;
length=4;
sys.path.append('F:\\')
#sys.path.append('/home/pi/credentials')
import config as cfg
reddit = praw.Reddit(client_id=cfg.client_id,
                     client_secret=cfg.client_secret,
                     user_agent=cfg.user_agent,
                     username=cfg.username,
                     password=cfg.password)
serial=serial.Serial('COM6',baudRate)
#serial=serial.Serial('/dev/ttyACM0',baudRate)
while True:
    modqueue = reddit.subreddit('mod').mod.modqueue(limit=None)
    value = len(list(modqueue))
    while(int(value)>9999):
        value=int(value)%10**(len(str(value))-1)
    value = "0"*(length-len(str(value)))+str(value)
    serial.write(str(value).encode())