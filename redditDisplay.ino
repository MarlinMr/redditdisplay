int number=0;
int baudRate=9600;
int firstdigitPin=2;
int lastdigitPin=5;
int firstSegmentPin=6;
int lastSegmentPin=12;
char sBuffer[4];
int readbytes=4;
int pow10[4] = {1000,100,10,1};
const byte aSegments[11][8] = {
//  A     B     C     D     E     F     G
{ HIGH, HIGH, HIGH, HIGH, HIGH, HIGH,  LOW }, // 0
{  LOW, HIGH, HIGH,  LOW,  LOW,  LOW,  LOW }, // 1
{ HIGH, HIGH,  LOW, HIGH, HIGH,  LOW, HIGH }, // 2
{ HIGH, HIGH, HIGH, HIGH,  LOW,  LOW, HIGH }, // 3
{  LOW, HIGH, HIGH,  LOW,  LOW, HIGH, HIGH }, // 4
{ HIGH,  LOW, HIGH, HIGH,  LOW, HIGH, HIGH }, // 5
{ HIGH,  LOW, HIGH, HIGH, HIGH, HIGH, HIGH }, // 6
{ HIGH, HIGH, HIGH,  LOW,  LOW,  LOW, LOW  }, // 7
{ HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH }, // 8
{ HIGH, HIGH, HIGH, HIGH,  LOW, HIGH, HIGH }, // 9
{  LOW,  LOW,  LOW,  LOW,  LOW,  LOW,  LOW }  // all off
};
void setup() {
  for(int segment=firstSegmentPin;  segment<=lastSegmentPin;  segment++){pinMode(segment,OUTPUT);}
  for(int digit=firstdigitPin;    digit<=lastdigitPin;    digit++) { 
    pinMode(      digit,  OUTPUT);
    digitalWrite( digit,  HIGH);
  }    
  Serial.begin(baudRate);
}
void drawdigit(int number){
    for(int segment=firstSegmentPin; segment<=lastSegmentPin; segment++){digitalWrite(segment,aSegments[number][segment-firstSegmentPin]);}
    for(int segment=firstSegmentPin; segment<=lastSegmentPin; segment++){digitalWrite(segment,LOW);}
}
void loop() {
  if(Serial.available()){Serial.readBytes(sBuffer, readbytes);number=atoi(sBuffer);}
  for(int digit=firstdigitPin; digit<=lastdigitPin; digit++){
    digitalWrite(digit,LOW);
    drawdigit((number/pow10[digit-firstdigitPin])%10);
    digitalWrite(digit,HIGH);
  }
}
